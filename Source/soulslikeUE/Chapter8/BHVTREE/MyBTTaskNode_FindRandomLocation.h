// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "MyBTTaskNode_FindRandomLocation.generated.h"

/**
 * 
 */
UCLASS()
class SOULSLIKEUE_API UMyBTTaskNode_FindRandomLocation : public UBTTaskNode
{
	GENERATED_BODY()
	
	protected:
		UPROPERTY(EditAnywhere, Category = Blackboard)
		FBlackboardKeySelector vRandomLocationKey {};

		UPROPERTY(EditAnywhere, Category = Parameters)
		float MaxSearchRadius	{300.f};

	public:
		/*
		*@brief Constructor to assign a default name to the node
		*/
		UMyBTTaskNode_FindRandomLocation(const FObjectInitializer& p_ObjectInitializer = FObjectInitializer::Get());

		/*
		*@brief Task implemented to Find a Random Location in a Radius. The origin is the controlled pawn.
		*/
		virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& p_OwnerCmp, uint8* NodeMemory) override;

		/*
		* @brief Method used to show the description of the Task and the variables used in it.
		*/
		virtual FString GetStaticDescription() const override;
};
