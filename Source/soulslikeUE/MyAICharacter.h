// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BehaviorTree/BehaviorTree.h"
#include "MyAICharacter.generated.h"

UCLASS()
class SOULSLIKEUE_API AMyAICharacter : public ACharacter
{
	GENERATED_BODY()

public:
	//**Bheavior Tree for an AIController (Added in chapter 2)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = AI)
		UBehaviorTree* BehaviorTree;

public:
	// Sets default values for this character's properties
	AMyAICharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
