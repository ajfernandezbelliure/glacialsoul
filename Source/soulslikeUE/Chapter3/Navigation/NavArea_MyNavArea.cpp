// Fill out your copyright notice in the Description page of Project Settings.


#include "NavArea_MyNavArea.h"

//=============================================================================

UNavArea_MyNavArea::UNavArea_MyNavArea()
{
	//Tengo que asignar el default cost, FixedAreaEnteringCost, DrawColor
	DefaultCost = 1.5f;
	FixedAreaEnteringCost = 4.5f;
	DrawColor = FColor::Yellow;
}
