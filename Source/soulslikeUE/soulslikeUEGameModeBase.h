// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "soulslikeUEGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SOULSLIKEUE_API AsoulslikeUEGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AsoulslikeUEGameModeBase();
	
};
