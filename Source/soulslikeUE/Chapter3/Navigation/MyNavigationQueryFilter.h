// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NavFilters/NavigationQueryFilter.h"
#include "MyNavigationQueryFilter.generated.h"

/**
 * 
 */
UCLASS()
class SOULSLIKEUE_API UMyNavigationQueryFilter : public UNavigationQueryFilter
{
	GENERATED_BODY()

	UMyNavigationQueryFilter();
};
