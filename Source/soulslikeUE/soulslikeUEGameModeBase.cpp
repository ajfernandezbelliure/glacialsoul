// Copyright Epic Games, Inc. All Rights Reserved.


#include "soulslikeUEGameModeBase.h"
#include "PlayerCharacter.h"
#include "UObject/ConstructorHelpers.h"


AsoulslikeUEGameModeBase::AsoulslikeUEGameModeBase()
{
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/LandsBetween/Blueprints/BP_PlayerCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
		DefaultPawnClass = PlayerPawnBPClass.Class;

	static ConstructorHelpers::FClassFinder<AController> PlayerControllerBPClass(TEXT("/Game/LandsBetween/Blueprints/BP_PlayerController"));
	if (PlayerControllerBPClass.Class != nullptr)
		PlayerControllerClass = PlayerControllerBPClass.Class;
}
