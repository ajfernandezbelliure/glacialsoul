// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTService_UpdateChasing.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include <soulslikeUE/Chapter2/AI/MyAIController.h>
#include "Kismet/GameplayStatics.h"


UMyBTService_UpdateChasing::UMyBTService_UpdateChasing(const 
	FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bNotifyBecomeRelevant = true;
	bNotifyCeaseRelevant = false;

	NodeName = "Upadte Chasing Behavior";

	//Adding filters so the user can only select KeySelectors of the specified type
	bCanSeePlayerKey.AddBoolFilter(this, 
						GET_MEMBER_NAME_CHECKED(UMyBTService_UpdateChasing, bCanSeePlayerKey));

	PlayerActorKey.AddObjectFilter(this,
						GET_MEMBER_NAME_CHECKED(UMyBTService_UpdateChasing, PlayerActorKey),
							AActor::StaticClass());

	LastKnownPlayerPositionKey.AddVectorFilter(this,
						GET_MEMBER_NAME_CHECKED(UMyBTService_UpdateChasing, LastKnownPlayerPositionKey));
}

//=============================================================================

void UMyBTService_UpdateChasing::OnBecomeRelevant(UBehaviorTreeComponent& p_OwnerCMP, uint8* p_NodeMemory)
{
	//Get the Blackboard Component
	UBlackboardComponent* BlackBoardCMP{p_OwnerCMP.GetBlackboardComponent()};
	if (BlackBoardCMP == nullptr)
		return;

	//Only update the value of the Blackboard if the key is not set
	if (!PlayerActorKey.IsSet())
	{
		//Retrieve the Player and Update the Blackboard
		TArray<AActor*> FoundActors{};
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), PlayerClass, FoundActors);

		if(FoundActors.Num()>0)
		{
			if (FoundActors[0])
			{
				BlackBoardCMP->SetValueAsObject(PlayerActorKey.SelectedKeyName, FoundActors[0]);
			}
		}
	}
}

//=============================================================================

void UMyBTService_UpdateChasing::TickNode(UBehaviorTreeComponent& p_OwnerCMP, uint8* p_NodeMemory, float p_DeltaSeconds)
{
	//Get the Blackboard Component
	UBlackboardComponent* BlackBoardCMP{ p_OwnerCMP.GetBlackboardComponent() };
	if (BlackBoardCMP == nullptr)
		return;
	
	//Get the AIController
	AAIController* OwnerAIController{p_OwnerCMP.GetAIOwner()};
	if (!OwnerAIController)
		return;

	//Get the MyAIController and check its validity.
	AMyAIController* MyAIController {Cast<AMyAIController>(OwnerAIController)};
	if (!MyAIController)
		return;

	//Update the blackboard key value for the CanSeePlayer value
	BlackBoardCMP->SetValueAsBool(bCanSeePlayerKey.SelectedKeyName,
							MyAIController->bCanSeePlayer);

	//If the LastCanSeePlayer is different from the current one, then update the LastKnownPlayerPosition
	if(MyAIController->bCanSeePlayer != bLastCanSeePlayer)
	{
		BlackBoardCMP->SetValueAsVector(LastKnownPlayerPositionKey.SelectedKeyName,
								MyAIController->LastKnownPlayerLocation);
	}

	//Update the LastCanSeePlayer to get the right value the next tick
	bLastCanSeePlayer = MyAIController->bCanSeePlayer;

	//Call tot he parent tick node
	Super::TickNode(p_OwnerCMP, p_NodeMemory, p_DeltaSeconds);
}

//=============================================================================

FString UMyBTService_UpdateChasing::GetStaticDescription() const
{
	FString ReturnDesc{ Super::GetStaticDescription() };
	ReturnDesc += "\n\n";
	ReturnDesc += FString::Printf(TEXT("%s: '%s'"), TEXT("PlayerClass: "),
		PlayerClass ? *PlayerClass->GetName() : TEXT("None"))
		.Append(FString::Printf(TEXT("\n%s: '%s'"), TEXT("PlayerKey"), PlayerActorKey.IsSet() ? *PlayerActorKey.SelectedKeyName.ToString() : TEXT(" ")))
		.Append(FString::Printf(TEXT("\n%s: '%s'"), TEXT("LastKnownPlayerPositionKey"), LastKnownPlayerPositionKey.IsSet() ? *LastKnownPlayerPositionKey.SelectedKeyName.ToString() : TEXT(" "))
		.Append(FString::Printf(TEXT("\n%s: '%s'"), TEXT("bCanSeePlayerKey"), bCanSeePlayerKey.IsSet() ? *bCanSeePlayerKey.SelectedKeyName.ToString() : TEXT(" ")))
		);

	return ReturnDesc;
}