// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NavAreas/NavArea.h"
#include "NavArea_MyNavArea.generated.h"

/**
 * 
 */
UCLASS()
class SOULSLIKEUE_API UNavArea_MyNavArea : public UNavArea
{
	GENERATED_BODY()

	/*
	* @brief Constructor for the type UNavArea_MyNavArea. In the book corresponds to the NavArea_Desert
	*/
	UNavArea_MyNavArea();
	
};
