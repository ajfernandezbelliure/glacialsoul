// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISense_Damage.h"
#include "Perception/AISenseConfig_Damage.h"
#include "MyAIController.generated.h"

/**
 * 
 */
UCLASS()
class SOULSLIKEUE_API AMyAIController : public AAIController
{
	GENERATED_BODY()

	protected:
		//PerceptionComponent variables
		UPROPERTY(VisibleDefaultsOnly, Category = AI)
		UAIPerceptionComponent* PerceptionComponent_{};

		UPROPERTY(BlueprintReadWrite, category=AIPerception);
		UAISenseConfig_Sight* SightConfig_{};

		UPROPERTY(BlueprintReadWrite, category = AIPerception);
		UAISenseConfig_Damage* DamageConfig_{};

	public:
		UPROPERTY(EditAnyWhere, Category = PerceptionKnowleadge)
		FVector LastKnownPlayerLocation{};

		UPROPERTY(EditAnyWhere, Category = PerceptionKnowleadge)
		bool bCanSeePlayer{};

	public:
		//Constructor
		AMyAIController();
	
	protected:
		/*
		* @brief Overrided version of the OnPossess function of the AICOntroller
		* 
		* @parameter APawn* InPawn. Te pawn that is going to be possessed.
		*/
		void OnPossess(APawn* InPAwn) override;

		/*
		* @brief Binding function to bind the OnTargetPerceptionUpdate to the AIController.
		* @parameters: 
		*	AACtor* pointer to the actor that triggers the stimuli
		*	FAIStimulus the stimulus that is perceived.
		*/
		UFUNCTION()
		void OnTargetPerceptionUpdate(AActor* InActor, FAIStimulus InStimulus);

	private:
		/*
		*@brief Method to configure the values of the Sight Sense
		*/
		void ConfigSightSense(float p_SightRadius, float p_LoseSightRadius, float p_HalfPeripheralAngle
								, float p_AutoSuccessRange, float p_MaxAge
								, bool p_DetectEnemies, bool p_DetecteNeutrals, bool p_DetectFriendlies);
		
		/*
		*@brief Method to configure the values of the Damage Sense
		*/
		void ConfigDamageSense(float p_MaxAge);

};
