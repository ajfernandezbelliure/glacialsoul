// Fill out your copyright notice in the Description page of Project Settings.


#include "MyNavigationQueryFilter.h"
#include "NavArea_MyNavArea.h"

//=============================================================================

UMyNavigationQueryFilter::UMyNavigationQueryFilter()
{
	//We need to create a Navigation Filter Area
	FNavigationFilterArea MyNavFilterArea {};

	//Now we need to fill the variables of that filter area
	MyNavFilterArea.AreaClass = UNavArea_MyNavArea::StaticClass(); //StaticClass() returns a UClass Object representing the clas at runtime
	
	MyNavFilterArea.bOverrideTravelCost = true;
	MyNavFilterArea.EnteringCostOverride = 0.f;
	
	MyNavFilterArea.bOverrideTravelCost = true;
	MyNavFilterArea.TravelCostOverride = 0.6f;

	//Finally we need to add this filter to the Array of Areas for the Filter
	Areas.Add(MyNavFilterArea);
}