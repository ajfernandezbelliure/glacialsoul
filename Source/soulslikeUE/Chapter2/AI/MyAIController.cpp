// Fill out your copyright notice in the Description page of Project Settings.

#include "MyAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include <soulslikeUE/MyAICharacter.h>
#include <soulslikeUE/PlayerCharacter.h>

AMyAIController::AMyAIController()
{
	//Create the PerceptionComponent_
	PerceptionComponent_ = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("BasePerceptionComponent"));
 
	//Create the SightConfig_
	SightConfig_ = CreateDefaultSubobject<UAISenseConfig_Sight>(FName("SightConfiguration"));
	DamageConfig_ = CreateDefaultSubobject<UAISenseConfig_Damage>(FName("DamageConfiguration"));

	//Configuring the Sight Sense
	ConfigSightSense(750.f, 1000.f, 90.f, -1.f, 5.f, true, true, false);

	//Configuring the Damage Sense
	ConfigDamageSense(5.f);

	//Assign the Sight to the PerceptionComponent
	PerceptionComponent_->ConfigureSense(*SightConfig_);

	//Assign the Damage to the PerceptionComponent
	PerceptionComponent_->ConfigureSense(*DamageConfig_);

	//Assign DominantSense
	PerceptionComponent_->SetDominantSense(DamageConfig_->GetSenseImplementation());

	//Binding the OnTargetPerceptionUpdate function
	PerceptionComponent_->OnTargetPerceptionUpdated.AddDynamic(this, &AMyAIController::OnTargetPerceptionUpdate);
}

//=============================================================================

void AMyAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	AMyAICharacter* PossessedCharacter { Cast<AMyAICharacter>(InPawn) };

	if (PossessedCharacter != nullptr) 
	{
		UBehaviorTree* BehaviorTreeToRun{ PossessedCharacter->BehaviorTree };
		if (BehaviorTreeToRun != nullptr) 
		{
			RunBehaviorTree(BehaviorTreeToRun);
		}
	}
}

//=============================================================================

void AMyAIController::OnTargetPerceptionUpdate(AActor* InActor, FAIStimulus InStimulus)
{
	//Check if the InActor is actually a PlayerCharacter
	if(Cast<APlayerCharacter>(InActor))
	{
		LastKnownPlayerLocation = InStimulus.StimulusLocation;
		bCanSeePlayer = InStimulus.WasSuccessfullySensed();
	}
}

//=============================================================================

void AMyAIController::ConfigSightSense(float p_SightRadius, float p_LoseSightRadius, float p_HalfPeripheralAngle
										, float p_AutoSuccessRange, float p_MaxAge
										, bool p_DetectEnemies, bool p_DetecteNeutrals, bool p_DetectFriendlies)
{
	SightConfig_->SightRadius = p_SightRadius;
	SightConfig_->LoseSightRadius = p_LoseSightRadius;
	SightConfig_->PeripheralVisionAngleDegrees = p_HalfPeripheralAngle;
	SightConfig_->AutoSuccessRangeFromLastSeenLocation = p_AutoSuccessRange;
	SightConfig_->SetMaxAge(p_MaxAge);
	SightConfig_->DetectionByAffiliation.bDetectEnemies = p_DetectEnemies;
	SightConfig_->DetectionByAffiliation.bDetectNeutrals = p_DetecteNeutrals;
	SightConfig_->DetectionByAffiliation.bDetectFriendlies = p_DetectFriendlies;
}

//=============================================================================

void AMyAIController::ConfigDamageSense(float p1)
{
	DamageConfig_->SetMaxAge(p1);
}


//=============================================================================
//OLD VERSION OF THE ONTARGETPERCEPTIONUPDATE
//=============================================================================

/*void AMyAIController::OnTargetPerceptionUpdate(AActor* InActor, FAIStimulus InStimulus)
{
	//Retrieve all the currently perceived actors in an Array
	TArray<AActor*> SightCurrentlyPerceivedActors{};
	PerceptionComponent_->GetPerceivedActors(TSubclassOf<UAISense_Sight>(), SightCurrentlyPerceivedActors);

	//Calculate the number of perceived Actors and if the current target Left or Entered the field of view
	bool bIsEntered{ SightCurrentlyPerceivedActors.Contains(InActor) };
	int NumberOfObjectsPerceived{ SightCurrentlyPerceivedActors.Num() };

	//Formating the string to print in the screen
	FString TextToPrint{ FString(InActor->GetName() + " has just " + (bIsEntered ? "Entered" : "Left")
								+ " the field of view.\n Now " + FString::FormatAsNumber(NumberOfObjectsPerceived)
								+ " objects are visible.") };
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TextToPrint);

	UE_LOG(LogTemp, Warning, TEXT("%s"), *TextToPrint);
}*/