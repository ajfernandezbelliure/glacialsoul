// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "MyBTService_UpdateChasing.generated.h"

/**
 * 
 */
UCLASS()
class SOULSLIKEUE_API UMyBTService_UpdateChasing : public UBTService
{
	GENERATED_BODY()

	public:
		UPROPERTY(EditAnywhere, Category = BlackboardKeys)
		FBlackboardKeySelector bCanSeePlayerKey{};

		UPROPERTY(EditAnywhere, Category = BlackboardKeys)
		FBlackboardKeySelector PlayerActorKey {};

		UPROPERTY(EditAnywhere, Category = BlackboardKeys)
		FBlackboardKeySelector LastKnownPlayerPositionKey {};

		UPROPERTY(EditAnywhere, Category = BlackboardKeys)
		TSubclassOf<AActor> PlayerClass{};

	private:
		bool bLastCanSeePlayer{};

	public:
		//TODO: No entiendo esto, por que le pasa esos argumentos al constructor???? Que son???
		UMyBTService_UpdateChasing(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	protected:
		virtual void OnBecomeRelevant(UBehaviorTreeComponent& p_OwnerCMP, uint8* p_NodeMemory) override;

		virtual void TickNode(UBehaviorTreeComponent& p_OwnerCMP, uint8* p_NodeMemory, float p_DeltaSeconds) override;

		virtual FString GetStaticDescription() const override;

};
