# Soulslike-UE

This project is my final degree project where I'm developing a small demo of a Soulslike in Unreal Engine 5.

## Description
In this project I am going to develop a small demo with the core mechanics of a Soulslike game in Unreal Engine 5.
I will program the mechanics for the player and three differet AI's. One of the AI's will be a basic enemy as the ones you can find in a normal level of Dark Souls or Elden Ring. Another one will be a final boss with differente phases and behaviours. And finally a player helper, an NPC that helps the player fight the enemies.
All of this is going to be developed with C++ and Blueprints.

## Project status
This project is redy to be submitted as my final degree project.
From now on I will be transitioning form blueprints to C++ the AI parts.

## Assets
In order to run the project sucessfully you will need to downlad the following assets and place them in the Content folder:

https://drive.google.com/file/d/12PveW7Zfd8HpkWp-YWMYcgYTBdusegq0/view?usp=share_link
https://drive.google.com/file/d/1D69IlcWYrYU8A7AdqQOPcLwqmX9ZxvL1/view?usp=share_link
https://drive.google.com/file/d/1nbLogLuB5g4FlkYXRx5vj2nmdFaW9Jak/view?usp=share_link
https://drive.google.com/file/d/1jTFUWx0MWsUFJVfPwhtmqjTdnauL_t6X/view?usp=share_link
https://drive.google.com/file/d/14SVwHXin2yKPkekCzQ2skWqwdQvZjajT/view?usp=share_link

Also, you will need to download this FX and place them in the folder: Content/Landsbetween:

https://drive.google.com/file/d/1ljkMQziD_Hq4FnVJSA0shTAQNegt9r7P/view?usp=share_link

