// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTDecorator_CheckBoolVariable.h"
#include "BehaviorTree/BlackboardComponent.h"

UMyBTDecorator_CheckBoolVariable::UMyBTDecorator_CheckBoolVariable(const FObjectInitializer& p_ObjectInitializer)
	: Super(p_ObjectInitializer)
{
	NodeName = "Check Bool Variable";

	//Adding a filter to the variable to check so the user can only select Bool Variables
	bVariableToCheck.AddBoolFilter(this, 
				GET_MEMBER_NAME_CHECKED(UMyBTDecorator_CheckBoolVariable, bVariableToCheck));
}


bool
UMyBTDecorator_CheckBoolVariable::CalculateRawConditionValue(UBehaviorTreeComponent& p_ownerCMP, uint8* p_NodeMemory) const
{
	//Get the blackboard component
	const UBlackboardComponent* BBCmp {p_ownerCMP.GetBlackboardComponent()};

	if(BBCmp == nullptr)
		return false;

	//Perform Bool Variable Check
	return BBCmp->GetValueAsBool(bVariableToCheck.SelectedKeyName);
}

//=============================================================================

FString UMyBTDecorator_CheckBoolVariable::GetStaticDescription() const
{
	FString ReturnDesc{ Super::GetStaticDescription() };
	ReturnDesc	+=	"\n\n";
	ReturnDesc	+=	FString::Printf(TEXT("%s: '%s'"), TEXT("Bool variable to check: "), 
					bVariableToCheck.IsSet() ? *bVariableToCheck.SelectedKeyName.ToString() : TEXT("None"));

	return ReturnDesc;
}

