// Fill out your copyright notice in the Description page of Project Settings.


#include "MyBTTaskNode_FindRandomLocation.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "NavigationSystem.h"
#include "AIController.h"

UMyBTTaskNode_FindRandomLocation::UMyBTTaskNode_FindRandomLocation(const FObjectInitializer& p_ObjectInitializer)
	: Super(p_ObjectInitializer)
{
	NodeName = "Find Random Location";

	//Add a filter to the Key Type to only accept vectors
	vRandomLocationKey.AddVectorFilter(this, 
							GET_MEMBER_NAME_CHECKED(UMyBTTaskNode_FindRandomLocation, vRandomLocationKey));
}

//=============================================================================

EBTNodeResult::Type 
UMyBTTaskNode_FindRandomLocation::ExecuteTask(UBehaviorTreeComponent& p_OwnerCmp, uint8* NodeMemory) 
{
	//Get BlackboardComponent
	UBlackboardComponent* BlackBoardCMP{p_OwnerCmp.GetBlackboardComponent()};

	if (BlackBoardCMP == nullptr)
		return EBTNodeResult::Failed;

	//Get ControlledPawn
	APawn* ControlledPawn{p_OwnerCmp.GetAIOwner()->GetPawn()};
	
	if(ControlledPawn == nullptr)
		return EBTNodeResult::Failed;

	//GetTheNavigationSystem
	UNavigationSystemV1* NavigationSystem{ UNavigationSystemV1::GetCurrent(GetWorld())};
	
	if (NavigationSystem == nullptr)
		return EBTNodeResult::Failed;

	//Prepare variables for query
	FNavLocation Result{};
	FVector Origin{ControlledPawn->GetActorLocation()};

	//Perform Query
	bool Success{NavigationSystem->GetRandomReachablePointInRadius(Origin, MaxSearchRadius, Result)};

	if(!Success)
		return EBTNodeResult::Failed;

	//Save Result and report success
	BlackBoardCMP->SetValueAsVector(vRandomLocationKey.SelectedKeyName, Result.Location);
	return EBTNodeResult::Succeeded;
}

//=============================================================================

FString UMyBTTaskNode_FindRandomLocation::GetStaticDescription() const
{
	FString ReturnDesc{ Super::GetStaticDescription() };
	ReturnDesc += "\n\n";
	ReturnDesc += FString::Printf(TEXT("%s: '%s'"), TEXT("DestinationKey: "),
		vRandomLocationKey.IsSet() ? *vRandomLocationKey.SelectedKeyName.ToString() : TEXT("None"))
		.Append (FString::Printf(TEXT("\n%s: '%s'"), TEXT("Radius"), *FString::SanitizeFloat(MaxSearchRadius)));

	return ReturnDesc;
}
