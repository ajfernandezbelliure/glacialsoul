// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "MyBTDecorator_CheckBoolVariable.generated.h"

/**
 * 
 */
UCLASS()
class SOULSLIKEUE_API UMyBTDecorator_CheckBoolVariable : public UBTDecorator
{
	GENERATED_BODY()
	
	protected:
		UPROPERTY(EditAnywhere, Category=Blackboard)
		FBlackboardKeySelector bVariableToCheck{};

	public:
		/*
		*@brief Constructor to assign a default name to the node
		*/
		UMyBTDecorator_CheckBoolVariable(const FObjectInitializer& p_ObjectInitializer = FObjectInitializer::Get());

		/*
		* @brief Method that checks if the variable set in the decorator is true or false. I
		* If there is no BlackBoardComponent will return false.
		*/
		virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& p_ownerCMP, uint8* p_NodeMemory) const override;

		/*
		* @brief Method used to show the description of which variable will be checked.
		*/
		virtual FString GetStaticDescription() const override;
};
